# Ilias Gkolfos - IoT Assignment

In the following project we created simple functionalities for the specified ST board, that are usefull for IoT purproses.
Sush functionalities were a UART communication between two boards, connected peripherals in order to read from and write
to them, as well as reading and write to boards memory. A Command Line Interface was also implemented to facilitate the
user who would use the aforementioned board and its functionalities.

This project was a part of the corresponding IoT course at University of Thessaly.
