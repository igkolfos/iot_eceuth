#! /bin/python3

##################################################
#       IoT Command Line Interface (CLI)         #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# Serial communitcation is performed by PySerial #
##################################################

import serial
import sys
import time
from datetime import datetime

DEFAULTPORTNAME = "/dev/ttyACM0"
TIMEOUT = 2 # sec
COMMANDSIZE = 3 # 3 bytes
TIMEOUTTOCLEARDATA = 1 # sec;
ADDRESSBITS = 8 # number of bytes to represent address
TIMEDATASIZE = 5 # number of bytes to represent address
             # max time = 23 * 3600 + 59 * 50 + 59 = 86399 sec, i.e. 5 chars max
TOTALATTEMPTSTORECEIVE = 10

DEBUGMESSAGES = 1 # show debug messages when set to 1

# dictionary for commands. <command args> : <description>
COMMANDS = {
    "exit"          : " ",
    "help"          : " ",
    "set_led"       : "<red|green|blue> <1|0> (Sets the corresponding led on or off)",
    "sense"         : "(Retrieve data from the available sensor peripherals)",
    "storage_write" : "<address> <data> (Write data to board's internal flash)",
    "storage_read"  : "<address> <data size> (Read data from board's internal flash)",
    "time_set"      : "?<hours> <minutes> <seconds>? (Set internal RTC time. If not specified, current time will be set)",
    "time_get"      : "(Get internal RTC time)",
    "sample"        : "(run a sample)"
}

# dictionary for opcodes
OPCODES = {
    "set_led"       : "1",
    "sense"         : "2",
    "storage_write" : "3",
    "storage_read"  : "4",
    "time_set"      : "5",
    "time_get"      : "6",
    "sample"        : "7"
}

# based on led.h
LEDCODES = {
    "red"   : "0",
    "green" : "1",
    "blue"  : "2"
}

def setup_port(portname):
    while(1):
        try:
            serialport = serial.Serial(port = portname, baudrate = 115200, bytesize = 8, 
                                        timeout = TIMEOUT, stopbits = serial.STOPBITS_ONE)
            
            break;
        except:
            printerror("Cannot find serial port " + portname)
            
            portname = input("Enter port name: ")

    # clear data
    serialport.reset_input_buffer()    
    serialport.reset_output_buffer()

    return serialport

def printinfo(str):
    print("INFO: " + str)

def printdbg(str):
    if (DEBUGMESSAGES == 1):
        print("DEBUG: " + str)

def printerror(str):
    print("ERROR: " + str)

def receive_from_board(serialport, datasize, attemptsnum, steptime):
    i = 0
    receiveddata = ""

    # wait for board's messages 
    # time.sleep(TIMEOUTTOCLEARDATA)

    printdbg("Flushing input buffer")

    # clear data from input buffer
    serialport.reset_input_buffer()

    # do a number of attempts to read data
    while(i < attemptsnum):

        if(serialport.in_waiting == 0):
            printdbg("No data available yet")
            
            time.sleep(steptime)
        else:
            try:
                serialdata = serialport.read(datasize)

                # receiveddata += serialdata.decode("utf-8")
                receiveddata += serialdata.decode("ascii")
                
                printdbg("\t\tReceived data = " + receiveddata)
                
                # check if all data arrived
                if(len(receiveddata) == datasize):
                    break

            except:
                printerror("Read interrupt")

                break
        i += 1

    printdbg("Received data: " + receiveddata)
    
    return receiveddata

def set_led(serialport, ledname, onoff):

    if((ledname != "red") and 
        (ledname != "green") and 
        (ledname != "blue")):
        printerror("Wrong led name: " + ledname)
        return

    if((onoff != "1") and (onoff != "0")):
        printerror("Wrong value to set led")
        return

    printinfo("Trying to set led " + ledname + " to " + onoff)

    # Send to the device connected over the serial port the data
    # The b at the beginning is used to indicate bytes
    # serialport.write(b"hi")
    command = OPCODES["set_led"] + LEDCODES[ledname] + onoff

    printdbg("Sending " + command)

    serialport.write(str(command).encode('ascii'))
    
    time.sleep(TIMEOUTTOCLEARDATA)
    
    # Receive "ACK" from board
    ackmessage = receive_from_board(serialport, COMMANDSIZE, TOTALATTEMPTSTORECEIVE, 0.5) # 10 attempts to read with 0.5 sec timeout
    
    if (ackmessage == "ACK"):
        printinfo("Success!")
    else:
        printinfo("Failure!")

def sense(serialport):
    sensedata = ""

    command = OPCODES["sense"]
    for i in range(1, COMMANDSIZE):
        command += "0"

    printdbg("Sending " + command)

    # send sense command
    serialport.write(str(command).encode('ascii'))

    time.sleep(TIMEOUTTOCLEARDATA)

    # Receive sense data from board
    sensedata = receive_from_board(serialport, COMMANDSIZE, TOTALATTEMPTSTORECEIVE, 0.5) # 10 attempts to read with 0.5 sec timeout

    if (sensedata == ""):
        printerror("No Sense Data")
    else:
        printinfo("Sensor feels = " + sensedata[0])

    return

def time_set(serialport, hours, minutes, seconds):
    receivedmessage = ""
    timetosend = ""
    timetosendcharsnum = 0
    hoursint = int(hours)
    minutesint = int(minutes)
    secondsint = int(seconds)
    totalseconds = 0

    if hoursint > 23:
        printerror("Too large hours value")
        return 1
    if minutesint > 59:
        printerror("Too large minutes value")
        return 1
    if secondsint > 59:
        printerror("Too large seconds value")
        return 1

    printinfo("Setting time " + str(hours) + "h " + str(minutes) + "m " + str(seconds) + "s")

    # calculate total time to send in seconds
    totalseconds = (hoursint * 3600) + (minutesint * 60) + secondsint

    timetosend = str(totalseconds).zfill(TIMEDATASIZE)

    # build command
    command = OPCODES["time_set"] + "".zfill(COMMANDSIZE - 1)

    printdbg("Sending " + command)

    # send sense command
    serialport.write(str(command).encode('ascii'))

    # wait for board to setup and receive again
    time.sleep(3) # 3 sec

    # step 2: send 4-bytes representation address
    printdbg("Sending time value " + str(hours) + "h " + str(minutes) + "m " + str(seconds) + "s. Value = " + timetosend)
    serialport.write(timetosend.encode('ascii'))

    printdbg("Wait to clear data")
    time.sleep(TIMEOUTTOCLEARDATA)

    # Receive sense data from board
    receivedmessage = receive_from_board(serialport, COMMANDSIZE, TOTALATTEMPTSTORECEIVE, 0.5) # 10 attempts to read with 0.5 sec timeout

    if (receivedmessage != "ACK"):
        printerror("No ACK received")
        return 1
    
    return

def time_get(serialport):
    receivedmessage = ""

    # build command
    command = OPCODES["time_get"] + "".zfill(COMMANDSIZE - 1)

    printdbg("Sending " + command)

    # send sense command
    serialport.write(str(command).encode('ascii'))

    # wait for board to setup and receive again    
    time.sleep(TIMEOUTTOCLEARDATA)

    # Receive sense data from board
    receivedmessage = receive_from_board(serialport, TIMEDATASIZE, TOTALATTEMPTSTORECEIVE, 0.5) # 10 attempts to read with 0.5 sec timeout
    if (receivedmessage == ""):
        printerror("Could NOT get time!")
        return

    receivedmessage = receivedmessage.replace('\x00', '')
    totalseconds = int(receivedmessage)

    hours = int(totalseconds / 3600)
    minutes = int((totalseconds - (3600 * hours)) / 60)
    seconds = int((totalseconds - (3600 * hours) - (minutes * 60)))

    printinfo("Board time is " + str(hours) + "h " + str(minutes) + "m " + str(seconds) + "s")

    return

# send storage_write opcode, followed by the size of the data to be stored
# flow:
#   1. send opcode + data size (3 bits)
#   2. send address (4 bits)
#   3. send data (99 bits max)
#   4. reveive ACK
# NOTE: specified address does NOT have to be the accurate address where the data
#       are stored; board stores the data at ADDR_FLASH_PAGE_16, 0x08010000, plus
#       the specified address, i.e. ADDR_FLASH_PAGE_16 + address
def storage_write(serialport, address, dataarray):
    receivedmessage = ""
    frontzeros = ""

    # convert address string to integer
    if ("0x" in address):
        # convert as a hex representation
        addressint = int(address, base=16)
    else:
        addressint = int(address)

    # convert again to string in order to avoid 0x... expressions
    addresstosend = str(addressint)

    if (len(addresstosend) > ADDRESSBITS):
        printerror("Give smaller address. Max address bits to send: " + ADDRESSBITS)
        return 1

    # fill address with zeros in the front if it is too small
    addresstosend = addresstosend.zfill(ADDRESSBITS)

    # convert data array of strings to a single string
    data = ' '.join(dataarray)

    datasize = len(data)

    # check data size can be sent with only 2 bits when first command is sent
    if (datasize > 99):
        printerror("Data too large. Please send less 99 bits")
        return 1

    printdbg("address = " + address + ", data = " + data + " ( # " + str(datasize) + ")")

    # create command; datasize must be COMMANDSIZE - 1 bits, so fill with zeros if needed
    command = OPCODES["storage_write"] + str(datasize).zfill(COMMANDSIZE - 1)

    printdbg("Sending " + command)

    # step 1: send storage_write command and data size
    serialport.write(str(command).encode('ascii'))

    # wait for board to setup and receive again
    time.sleep(3) # 3 sec

    # step 2: send 4-bytes representation address
    printdbg("Sending address : " + addresstosend)
    serialport.write(addresstosend.encode('ascii'))
    
    # wait for board to setup and receive again
    time.sleep(3) # 3 sec

    # step 3: send data to be writen
    serialport.write(data.encode('ascii'))
    printdbg("Sent data: " + data)
    
    # step 4: wait for ACK
    time.sleep(1) # 1 sec
    receivedmessage = receive_from_board(serialport, COMMANDSIZE, TOTALATTEMPTSTORECEIVE, 0.5) # 10 attempts with 0.5 sec difference
    
    if (receivedmessage != "ACK"):
        printerror("No ACK received")
        return 1
    else:
        printinfo("Successful write @" + addressint + " = '" + data + "', length = " + str(datasize))

    return 0

# send storage_write opcode, followed by the size of the data to be stored
# flow:
#   1. send opcode + data size (3 bits)
#   2. send address (4 bits)
#   3. reveive ACK
# NOTE: specified address does NOT have to be the accurate address where the data
#       are stored; board stores the data at ADDR_FLASH_PAGE_16, 0x08010000, plus
#       the specified address, i.e. ADDR_FLASH_PAGE_16 + address
def storage_read(serialport, address, size):
    receivedmessage = ""
    frontzeros = ""

    printdbg("Read address: " + address)

    # convert address string to integer
    addressint = int(address)

    # convert again to string in order to avoid 0x... expressions
    addresstosend = str(addressint)

    if (len(addresstosend) > ADDRESSBITS):
        printerror("Give smaller address. Max address bits to send: " + ADDRESSBITS)
    else:
        for i in range(len(addresstosend), ADDRESSBITS):
            frontzeros += "0"

    # fill address with zeros in the front if it is too small
    addresstosend = frontzeros + addresstosend

    datasize = int(size)

    # check data size can be sent with only 2 bits when first command is sent
    if (datasize > 99):
        printerror("Data too large. Please send less 99 bits")
        return 1

    printdbg("address = " + address + " (bits = " + str(ADDRESSBITS) + ") data size = " + str(datasize))

    # command = OPCODES["storage_write"] + str(addressrequiredbits)
    command = OPCODES["storage_read"] + str(datasize).zfill(COMMANDSIZE - 1) # datasize must be COMMANDSIZE - 1 bits, so fill with zeros if needed

    printdbg("Sending " + command)

    # step 1: send storage_write command and data size
    serialport.write(str(command).encode('ascii'))

    # wait for board to setup and receive again
    time.sleep(3) # 3 sec

    # step 2: send 4-bytes representation address
    printdbg("Sending address " + addresstosend)
    serialport.write(addresstosend.encode('ascii'))

    # wait for board to setup and receive again
    time.sleep(2) # 3 sec

    # receive data
    receivedmessage = receive_from_board(serialport, int(size), 10, 0.5) # 10 attempts with 0.5 sec difference
    if (receivedmessage == ""):
        printerror("Could not read data from board")
    else:
        printinfo("Received = " + receivedmessage);

    return receivedmessage

# send sample opcode to run board's sample
def sample(serialport):
    # Send to the device connected over the serial port the data
    # The b at the beginning is used to indicate bytes
    # serialport.write(b"hi")
    command = OPCODES["sample"] + "".zfill(COMMANDSIZE - 1)

    printdbg("Sending " + command)

    serialport.write(str(command).encode('ascii'))
    
    return

def main(argv):

    print("#################################")
    print("#            IoT CLI            #")
    print("#################################\n")

    if (len(argv) == 1):
        portname = DEFAULTPORTNAME
    else:
        portname = str(argv[1])
    printinfo("Run at port: " + portname + "\n")

    # setup serial port
    serialport = setup_port(portname)

    # user command line loop
    while(1):
        command = input("IoTCLI> ")

        commandargv = command.split()

        if(len(commandargv) == 0):
            continue    
        elif(commandargv[0].lower() == "exit"):
            break
        elif(commandargv[0].lower() == "help"):
            
            print("Commands:")
            for cmd, description in COMMANDS.items():
                print("\t" + cmd + "\t: " + description)

        elif(commandargv[0].lower() == "set_led"):

            if(len(commandargv) != 3):
                printerror("Wrong args!\nSyntax: set_led " + COMMANDS["set_led"])
                continue

            set_led(serialport, commandargv[1], commandargv[2])

        elif(commandargv[0].lower() == "sense"):
            sense(serialport)
        elif(commandargv[0].lower() == "storage_write"):
            if (len(commandargv) < 3):
                printerror("Wrong args!\nSyntax: storage_write " + COMMANDS["storage_write"])
                continue

            storage_write(serialport, commandargv[1], commandargv[2:])
        elif(commandargv[0].lower() == "storage_read"):
            if (len(commandargv) != 3):
                printerror("Wrong args!\nSyntax: storage_read " + COMMANDS["storage_read"])
                continue

            storage_read(serialport, commandargv[1], commandargv[2])

        elif(commandargv[0].lower() == "time_set"):
            if (len(commandargv) == 1):
                # set current time
                now = datetime.now()

                time_set(serialport, now.hour, now.minute, now.second)

            elif (len(commandargv) == 4):
                time_set(serialport, commandargv[1], commandargv[2], commandargv[3])

            else:
                printerror("Wrong args!\nSyntax: time_set " + COMMANDS["time_set"])
                continue


        elif(commandargv[0].lower() == "time_get"):
            time_get(serialport)

        elif(commandargv[0].lower() == "sample"):
            sample(serialport)

        elif(commandargv[0].lower() == "debug"):
            # printinfo("Not implemented yet!")
            if (len(commandargv) != 2):
                continue

            global DEBUGMESSAGES

            if (commandargv[1] == '0'):
                DEBUGMESSAGES = 0
                print("Setting debug messages " + str(DEBUGMESSAGES))

            elif (commandargv[1] == '1'):
                DEBUGMESSAGES = 1
                print("Setting debug messages " + str(DEBUGMESSAGES))

        # do random tests
        elif(commandargv[0].lower() == "test"):

            storage_write(serialport, "1", "1")
            time.sleep(5)
            storage_read(serialport, "1")
            continue


        else:
            printerror("Unknown command: " + commandargv[0])

    if (serialport.is_open == True):
        serialport.close()

    sys.exit(0)


if __name__ == "__main__":
    main(sys.argv)