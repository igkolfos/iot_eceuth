#ifndef __LOG_H__
#define __LOG_H__

#include <stdint.h>

#define LOG_BUFFER_SIZE 256U

#define LOG_LEVEL_NONE 0U
#define LOG_LEVEL_ERR  1U
#define LOG_LEVEL_WRN  2U
#define LOG_LEVEL_INF  3U
#define LOG_LEVEL_DBG  4U

#ifndef LOG_LEVEL
    // define default log level
    #define LOG_LEVEL LOG_LEVEL_DBG
#endif

typedef uint8_t log_level_t;
void log_level(const log_level_t level,  const char *level_str, const char *format, ...); // should not use this directly!

#define LOG_ERR(...) log_level(LOG_LEVEL_ERR, "ERR", __VA_ARGS__);
#define LOG_WRN(...) log_level(LOG_LEVEL_WRN, "WRN", __VA_ARGS__);
#define LOG_INF(...) log_level(LOG_LEVEL_INF, "INF", __VA_ARGS__);
#define LOG_DBG(...) log_level(LOG_LEVEL_DBG, "DBG", __VA_ARGS__);

#endif /* __LOG_H__ */
