#include <stdarg.h>
#include <stdio.h>
#include "log.h"

#define MAX_SIZE (LOG_BUFFER_SIZE - 1) // should have space for \0

/**
 * Function for printing logs.
 * @param[in] level Desired level.
 * @param[in] level_str Desired level string prefix.
 * @param[in] format Provided output format.
 */
void log_level(const log_level_t level, const char *level_str, const char *format, ...) {
	if (LOG_LEVEL < level) {
		return;
	}

	char log_buffer[LOG_BUFFER_SIZE];
	uint16_t length = 0;

  	va_list list;
  	va_start(list, format);
	length += snprintf(&log_buffer[length], (MAX_SIZE - length), "%s: ", level_str);
  	length += vsnprintf(&log_buffer[length], (MAX_SIZE - length), format, list);
  	log_buffer[length++] = '\0';
  	va_end(list);
	printf("\r%s", log_buffer);
}