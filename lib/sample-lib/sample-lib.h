#ifndef __SAMPLE_LIB_H__
#define __SAMPLE_LIB_H__

/**
 * @brief Sample application that periodically blinks. Does not exit.
 */
void hello_world();

void set_active_buzzer();

void reset_active_buzzer();

GPIO_PinState read_peripheral();

int ping_pong(UART_HandleTypeDef *);

#endif /* __SAMPLE_LIB_H__ */
