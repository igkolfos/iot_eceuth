#include <stdio.h>
#include "main.h"
#include "led.h"
#include "communication.h"
#include "sample-lib.h"
#include <string.h>

#define BLINK_DURATION 500 // ms
#define BLINK_DELAY 400 // ms
#define BUFFER_SIZE 50

void blink() {
	LOG_INF("BLINK!\n");
	set_led(LED_ALL, LED_OFF);
	HAL_Delay(BLINK_DURATION / 4);
	set_led(LED_RED, LED_ON);
	HAL_Delay(BLINK_DURATION / 4);
	set_led(LED_RED, LED_OFF);
	set_led(LED_GREEN, LED_ON);
	HAL_Delay(BLINK_DURATION / 4);
	set_led(LED_GREEN, LED_OFF);
	set_led(LED_BLUE, LED_ON);
	HAL_Delay(BLINK_DURATION / 4);
	set_led(LED_BLUE, LED_OFF);
}

void hello_world() {
	blink();
	HAL_Delay(BLINK_DELAY); // IDLE for some time
}

void set_active_buzzer()
{
    HAL_GPIO_WritePin(PERIPHERAL_OUT_GPIO_Port, PERIPHERAL_OUT_Pin, GPIO_PIN_SET);
}

void reset_active_buzzer()
{
    HAL_GPIO_WritePin(PERIPHERAL_OUT_GPIO_Port, PERIPHERAL_OUT_Pin, GPIO_PIN_RESET);
}

GPIO_PinState read_peripheral()
{
    return HAL_GPIO_ReadPin(PERIPHERAL_IN_GPIO_Port, PERIPHERAL_IN_Pin);
}

int ping_pong(UART_HandleTypeDef *uart)
{
	char RxBuffer[BUFFER_SIZE];
	char Tx_DATA_B1[] = "PING";
	char Tx_DATA_B2[] = "PONG";
	GPIO_PinState bitstatus; // GPIO push button return value //
	int i;
	int test = 0;

	strcpy(RxBuffer,"\0");

	#ifdef TRANSMITTER_BOARD // Transmitter board
		/* Wait for User push-button (Start_Button_Pin) press before starting the Communication.
		In the meantime, LED_RED is blinking */
		LOG_INF("[B1] Push Transmitter button to start Transmitter\n\r");

		while(1)
		{
			bitstatus = HAL_GPIO_ReadPin(Start_Button_GPIO_Port, Start_Button_Pin);
			// LOG_DBG("[B1] Button status = %d\n", bitstatus);

			// when start button is set as "pull-up", so when the button is NOT pushed, GPIO_PIN_SET //
			// is returned and GPIO_PIN_RESET is returned when button is pushed;                     // 
			if(bitstatus == GPIO_PIN_RESET)
			{
				LOG_INF("[B1] Button pushed\n");
				break;
			}

			HAL_Delay(200); // button debouncing //

			/* Toggle LED_RED*/
			set_led(LED_RED, LED_OFF);
			HAL_Delay(100);
			set_led(LED_RED, LED_ON);
		}

		LOG_INF("[B1] \t### Start Transmitting ###\n");

	#else // Receiver board
		/* Wait for User push-button (Start_Button_Pin) press before starting the Communication.
		In the meantime, LED_BLUE is blinking */
		LOG_INF("[B2] Push Receiver button to start Receiving\n\r");

		while(1)
		{
			bitstatus = HAL_GPIO_ReadPin(Start_Button_GPIO_Port, Start_Button_Pin);
			// LOG_DBG("[B2] Button status = %d\n", bitstatus);

			// when start button is set as "pull-up", so when the button is NOT pushed, GPIO_PIN_SET //
			// is returned and GPIO_PIN_RESET is returned when button is pushed;                     // 
			if(bitstatus == GPIO_PIN_RESET)
			{
			LOG_INF("[B2] Button pushed\n");
			break;
			}

			HAL_Delay(200); // button debouncing //

			/* Toggle LED_BLUE*/
			set_led(LED_BLUE, LED_OFF);  
			HAL_Delay(100);
			set_led(LED_BLUE, LED_ON);
		}

		LOG_INF("[B2] \t### Starting Receiving ###\n");

	#endif

	while (1)
	{
		#ifdef TRANSMITTER_BOARD
			/*##-1- Start the transmission process #####################################*/
			/* While the UART in reception process, user can transmit data through "Tx_DATA_B1" buffer */

			LOG_INF("[B1] Transmitting %s\n", Tx_DATA_B1);

			transmit_message(uart, (uint8_t *) Tx_DATA_B1, strlen(Tx_DATA_B1) + 1, TRANSMITTER_TIMEOUT);

			HAL_Delay(RECEIVER_TIMEOUT/2);

			/*##-2- Start the reception process #####################################*/

			i = 0;

			test = receive_message(uart, (uint8_t *) RxBuffer, COUNTOF(Tx_DATA_B1), RECEIVER_TIMEOUT);

			// no data received //
			while (test != 0 )
			{
				HAL_Delay(100);

				test = receive_message(uart, (uint8_t *) RxBuffer, COUNTOF(Tx_DATA_B1), RECEIVER_TIMEOUT);

				// abort after 10 attempts //
				if (i > 10)
				{
				break;
				}

				i++;
			}

			// received PONG //
			if (test == 0)
			{
				LOG_INF("\t## [B2] Received %s ##\n", (uint8_t *) RxBuffer);
			}

		#else
			/*##-1- Put UART peripheral in reception process ###########################*/

			// LOG_DBG("[B2] Waiting to receive...\n");

			test = receive_message(uart, (uint8_t *) RxBuffer, COUNTOF(Tx_DATA_B1), RECEIVER_TIMEOUT);
			while(test != 0)
			{
				// LOG_DBG("\t[B2] Received %s!\n", RxBuffer);

				HAL_Delay(10);

				strcpy(RxBuffer,"\0");

				test = receive_message(uart, (uint8_t *) RxBuffer, COUNTOF(Tx_DATA_B1), RECEIVER_TIMEOUT);
			}

			LOG_INF("\t## [B2] Received %s ##\n", RxBuffer);

			strcpy(RxBuffer,"\0");

			HAL_Delay(RECEIVER_TIMEOUT);

			/*##-2- Start the transmission process #####################################*/

			LOG_INF("[B2] Transmitting %s\n", Tx_DATA_B2);

			transmit_message(uart, (uint8_t *) Tx_DATA_B2, strlen(Tx_DATA_B2) + 1, TRANSMITTER_TIMEOUT);

		#endif
	}
}