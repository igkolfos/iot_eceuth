#ifndef __TIME_H__
#define __TIME_H__

/**
 * @brief Time setter. Sets the system's time.
 * 
 * @param[in] time The seconds from Epoch time.
 * @return int The result status of the operation.
 */
int time_set(uint32_t time);

/**
 * @brief Time getter. Returns the system's time.
 * 
 * @param[out] time The seconds from Epoch time.
 * @return int The result status of the operation.
 */
int time_get(uint32_t *time);

#endif /* __TIME_H__ */
