#include <stdint.h>
#include <errno.h>
#include "main.h"
#include "rtc.h"
#include "time.h"

// time is in seconds
int time_set(uint32_t time) {
    RTC_TimeTypeDef sTime = {0};
    uint8_t hours = 0;
    uint8_t minutes = 0;
    uint8_t seconds = 0;
    
    hours = time / 3600;
    minutes = (time - (3600 * hours)) / 60;
    seconds = (time - (3600 * hours) - (minutes * 60));

    if ((hours > 23) || (minutes > 59) || (seconds > 59))
    {
        LOG_ERR("Wrong time format. Too large numbers! - %dh %dm %ds\n", hours, minutes, seconds);
        return -ENOTSUP;
    }

    LOG_DBG("Time defined %ds\n", time);
    LOG_DBG("Set time to %dh %dm %ds\n", hours, minutes, seconds);

    // NOTE: RTC format is 24
    sTime.Hours = hours;
    sTime.Minutes = minutes;
    sTime.Seconds = seconds;
    sTime.SubSeconds = 0x0;
    sTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
    sTime.StoreOperation = RTC_STOREOPERATION_RESET;

    if (HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BIN) != HAL_OK)
    {
        LOG_ERR("Could NOT set time!\n");
        return -ENOTSUP;
    }

    return 0;
}

// time is in seconds
int time_get(uint32_t *time) {
    RTC_DateTypeDef sdatestructureget;
    RTC_TimeTypeDef stimestructureget;

    /* Get the RTC current Time */
    HAL_RTC_GetTime(&hrtc, &stimestructureget, RTC_FORMAT_BIN);
    /* Get the RTC current Date */
    HAL_RTC_GetDate(&hrtc, &sdatestructureget, RTC_FORMAT_BIN);
 
    /* Display time Format : hh:mm:ss */
    // sprintf((char *)time, "%02d:%02d:%02d", stimestructureget.Hours, stimestructureget.Minutes, stimestructureget.Seconds);
    LOG_DBG("Read Time %d:%d:%d\n", stimestructureget.Hours, stimestructureget.Minutes, stimestructureget.Seconds);

    *time = (stimestructureget.Hours * 3600) + (stimestructureget.Minutes * 60) + stimestructureget.Seconds;

    return 0;
}
