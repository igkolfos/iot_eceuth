#include "main.h"
#include "led.h"
#include <string.h>

int transmit_message(UART_HandleTypeDef *uart, uint8_t *message, uint16_t messagesize, int timeout)
{
	HAL_StatusTypeDef uart_return_value = HAL_ERROR;

	// LOG_DBG("Transmitting %s\n", message);

	uart_return_value = HAL_UART_Transmit(uart, (uint8_t *)message, messagesize, timeout); 
	
	if(uart_return_value != HAL_OK)
	{
		LOG_DBG("[ERROR] uart return value %d\n", uart_return_value);

		return 1;
	}

	return 0;
}

int receive_message(UART_HandleTypeDef *uart, uint8_t *message, uint16_t messagesize, int timeout)
{
	HAL_StatusTypeDef uart_return_value = HAL_ERROR;
	
	// found from https://community.st.com/s/question/0D50X00009XkYGPSA3/haluartreceive-timeout-issue
	__HAL_UART_CLEAR_OREFLAG(uart);
    __HAL_UART_CLEAR_NEFLAG(uart);

	uart_return_value = HAL_UART_Receive(uart, (uint8_t *)message, messagesize, timeout);
	
	if(uart_return_value != HAL_OK)
	{
		// LOG_DBG("## ERROR ## uart return value %d\t tempreceive = %s!\n", uart_return_value, tempreceive);

		return 1;
	}

	return 0;
}
