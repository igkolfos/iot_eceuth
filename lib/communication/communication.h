#ifndef __COMMUNICATION_H__
#define __COMMUNICATION_H__

#define TRANSMITTER_TIMEOUT 500 // ms
#define RECEIVER_TIMEOUT 400 // ms
#define COUNTOF(__BUFFER__)   (sizeof(__BUFFER__) / sizeof(*(__BUFFER__)))

int transmit_message(UART_HandleTypeDef *uart, uint8_t *message, uint16_t messagesize, int timeout);
int receive_message(UART_HandleTypeDef *uart, uint8_t *message, uint16_t messagesize, int timeout);


#endif /* __COMMUNICATION_H__ */