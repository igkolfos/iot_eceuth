#include <stdio.h>
#include <stdint.h>
#include <errno.h>
#include "main.h"
#include "cmd.h"
#include "led.h"
#include "log.h"
#include "communication.h"
#include "storage.h"
#include "time.h"
#include "sample-lib.h"
#include "usart.h"
#include <string.h>
#include <stdlib.h>

//  Commands      |  Encoding
// ------------------------
//   no_op        |    0
//  set_led       |    1
//   sense        |    2
// storage_write  |    3
// storage_read   |    4
//  time_set      |    5
//  time_get      |    6
void get_param_index_char(void *data, int index, char *param)
{
    if (param == NULL)
    {
        return;
    }

    *param = *(char *)(data + index * sizeof(char));
    LOG_DBG("\tdata %s, index %d, param %c\n", data, index, *param);
}

void get_param_index_int(void *data, int index, int *param)
{
    if (param == NULL)
    {
        return;
    }

    *param = *(int *)(data + index * sizeof(int));
    LOG_DBG("\tdata %s, index %d, param %d\n", data, index, *param);
}

int cmd_noop(void *unused) {
    ARG_UNUSED(unused);
    LOG_INF("NOOP");
    return 0;
}

int cmd_time_get(void *data)
{
    int timeread = 0;
    char timetosend[99] = {'\0'};

    time_get((uint32_t *) &timeread);

    sprintf(timetosend, "%d", timeread);

    LOG_DBG("Time read is %ds (string = %s)\n", timeread, timetosend);

    HAL_Delay(TIMEOUTBEFOREPC); // wait for PC to clear its buffers

    transmit_message(&huart1, (uint8_t *) timetosend, TIMEDATASIZE, TRANSMITTER_TIMEOUT);

    return 0;
}

// receive time to set as total seconds
//  1. receive time (5 bits)
//  2. write data to internal flash
//  3. send ACK
int cmd_time_set(void *data)
{
    char cmdreturndata[COMMANDSIZE + 1];
    char receivedtime[9] = {'\0'};
    uint32_t time = 0;
    int timeisset = 0;

    // wait to receive time to set
    HAL_Delay(TIMEOUTBEFOREPC);

    receive_message(&huart1, (uint8_t *) receivedtime, TIMEDATASIZE, 5000); // 5 sec to receive address as 8 bytes string

    LOG_DBG("Received time = %d\n", receivedtime);
    time = atoi(receivedtime);

    timeisset = time_set(time);
    
    if (timeisset == 0) // success
    {
        strcpy(cmdreturndata, "ACK");
    }
    else
    {
        strcpy(cmdreturndata, "ERR");
    }
    
    HAL_Delay(TIMEOUTBEFOREPC); // wait for PC to clear its buffers

    transmit_message(&huart1, (uint8_t *) cmdreturndata, COMMANDSIZE, TRANSMITTER_TIMEOUT);

    return 0;
}

// parse and store specified data to internal flash
// flow:
//  1. already received opcode + data size (1 + 2 bits)
//      a. data configuration
//          i.data[0] = opcode num
//          ii. data[1:2] = data size
//  2. receive address (8 bits)
//  3. receive data (99 bits max)
//  4. write data to internal flash
//  5. send ACK
int cmd_storage_write(void *data) {
    char receivedaddress[9] = {'\0'};
    int addresstowrite = 0;
    int datasize = 0;
    char datastring[3] = {'\0'};
    char receiveddata[99] = {'\0'};
    // char testdata[] = "test";
    
    // Step 1 //

    datastring[0] = *(char *)(data + 1 * sizeof(char));
    datastring[1] = *(char *)(data + 2 * sizeof(char));
    datastring[2] = '\0';
    datasize = atoi(datastring);

    LOG_DBG("Data size = %d\n", datasize);

    // Step 2 //
    
    HAL_Delay(TIMEOUTBEFOREPC);

    receive_message(&huart1, (uint8_t *) receivedaddress, 8, 5000); // 5 sec to receive address as 8 bytes string

    addresstowrite = atoi(receivedaddress);
    
    LOG_DBG("Received Address = %d\n", addresstowrite);

    // Step 3 //
    
    HAL_Delay(TIMEOUTBEFOREPC);

    receive_message(&huart1, (uint8_t *) receiveddata, datasize, 5000); // 5 sec to receive data

    LOG_DBG("Received data = %s\n", receiveddata);

    // Step 4 //

    // write to flash //
    LOG_DBG("Calling storage_write(%d, %s, %d)\n", addresstowrite,receiveddata, strlen(receiveddata));
    storage_write(addresstowrite, (uint8_t *) receiveddata, strlen(receiveddata));
    
    // Step 5 //
    
    HAL_Delay(TIMEOUTBEFOREPC); // wait for PC to clear its buffers

    transmit_message(&huart1, (uint8_t *) "ACK", strlen("ACK") + 1, TRANSMITTER_TIMEOUT);

    return 0;
}

// parse and read data from internal flash
// flow:
//  1. already received opcode + data size (1 + 2 bits)
//      a. data configuration
//          i.data[0] = opcode num
//          ii. data[1:2] = data size
//  2. receive address (8 bits)
//  3. read data from internal flash
//  3. send back data
int cmd_storage_read(void *data)
{
    char receivedaddress[9] = {'\0'};
    int address = 0;
    int datasize = 0;
    char datastring[3] = {'\0'};
    char *readdata = NULL;
    // char readdata[99] = {'\0'};

    // Step 1 //

    datastring[0] = *(char *)(data + 1 * sizeof(char));
    datastring[1] = *(char *)(data + 2 * sizeof(char));
    datastring[2] = '\0';
    datasize = atoi(datastring);

    LOG_DBG("Data size string = %s, int = %d\n", datastring, datasize);

    // Step 2 //
    HAL_Delay(TIMEOUTBEFOREPC);

    receive_message(&huart1, (uint8_t *) receivedaddress, 8, 5000); // 5 sec to receive address as 9 bytes string

    address = atoi(receivedaddress);
    LOG_DBG("Received Address string = %s, int = %d\n", receivedaddress, address);

    // Step 3 //

    readdata = calloc(datasize, sizeof(char));

    storage_read((uint32_t) address, (uint8_t *) readdata, (uint32_t) datasize);
    
    LOG_DBG("\tFlash read data = %s.\n", readdata);

    // Step 4 //
    LOG_DBG("\tWaiting %dms to transmit back data = %s.\n", TIMEOUTBEFOREPC, readdata);

    HAL_Delay(TIMEOUTBEFOREPC); // wait for PC to clear its buffers

    transmit_message(&huart1, (uint8_t *) readdata, datasize, TRANSMITTER_TIMEOUT);

    free(readdata);

    return 0;
}

int cmd_sense(void *data) {
    char cmdreturndata[COMMANDSIZE + 1];
    int sensordata;

    sensordata = read_peripheral();
    
    if (sensordata == 0)
    {
        strcpy(cmdreturndata, "000");
    }
    else
    {
        strcpy(cmdreturndata, "100");
    }

    HAL_Delay(TIMEOUTBEFOREPC); // wait for PC to clear its buffers

    transmit_message(&huart1, (uint8_t *) cmdreturndata, COMMANDSIZE, TRANSMITTER_TIMEOUT);

    return 0;
}

int cmd_set_led(void *data) {
    char ledcode;
    char statecode;
    led_t led;
    led_state_t state;

    // data[1] == led, data[2] == 1/0 (on/off)
    get_param_index_char(data, 1, &ledcode);
    get_param_index_char(data, 2, &statecode);

    switch(ledcode)
    {
        // red //
        case('0'):
            led = LED_RED;
            break;
        // green //
        case('1'):
            led = LED_GREEN;
            break;
        // blue //
        case('2'):
            led = LED_BLUE;
            break;
        default:
            led = LED_ALL;
            break;
    }

    switch(statecode)
    {
        case('0'):
            state = LED_OFF;
            break;
        case('1'):
            state = LED_ON;
            break;
        default:
            state = LED_OFF;
            break;
    }

    LOG_DBG("Setting led %d to %d\n", led, state);

    set_led(led, state);
    
    HAL_Delay(TIMEOUTBEFOREPC); // wait for PC to clear its buffers

    // Send "ACK" to PC
    transmit_message(&huart1, (uint8_t *) "ACK", COMMANDSIZE, TRANSMITTER_TIMEOUT);

    HAL_Delay(TIMEOUTBEFOREPC);
    
    return 0;
}

int cmd_sample(void *data) {
    LOG_DBG("Running sample\n");

    LOG_ERR("No sample funcion is specified! Call a sample function at file cmd.c:cmd_sample() function\n");
    
    return 0;
}
int cmd_exec(cmd_def_t list[], cmd_id_t id, void *data) {
    cmd_def_t *entry;
    for (entry = list; entry->id != CMD_END_ID; entry++) {
        if (entry->id == id) return entry->handler(data);
    }
    return -ENOTSUP;
}

int cmd_read_data(char *data) {
    char cmdopcode;
    int cmdop;

    enum {
        CMD_NOOP_ID = 0x00,
        CMD_SET_LED_ID = 0x01,
        CMD_SENSE_ID = 0x02,
        CMD_STORAGE_WRITE_ID = 0x03,
        CMD_STORAGE_READ_ID = 0x04,
        CMD_TIME_SET_ID = 0x05,
        CMD_TIME_GET_ID = 0x06,
        CMD_SAMPLE_ID = 0x07
    };
    
    cmd_def_t cmd_def_list[] = {
        {.id = CMD_NOOP_ID, .handler          = (cmd_handler_fn)cmd_noop},
        {.id = CMD_SET_LED_ID, .handler       = (cmd_handler_fn)cmd_set_led},
        {.id = CMD_SENSE_ID, .handler         = (cmd_handler_fn)cmd_sense},
        {.id = CMD_STORAGE_WRITE_ID, .handler = (cmd_handler_fn)cmd_storage_write},
        {.id = CMD_STORAGE_READ_ID, .handler  = (cmd_handler_fn)cmd_storage_read},
        {.id = CMD_TIME_SET_ID, .handler      = (cmd_handler_fn)cmd_time_set},
        {.id = CMD_TIME_GET_ID, .handler      = (cmd_handler_fn)cmd_time_get},
        {.id = CMD_SAMPLE_ID, .handler        = (cmd_handler_fn)cmd_sample},
        {.id = CMD_END_ID} // this must always be last
    };
    
    get_param_index_char(data, 0, &cmdopcode);
    LOG_DBG("cmdopcode = %c\n", cmdopcode);

    switch(cmdopcode)
    {
        // no op //
        case('0'):
            cmdop = CMD_NOOP_ID;
            break;
        // set_led //
        case('1'):
            cmdop = CMD_SET_LED_ID;
            break;
        // sense //
        case('2'):
            cmdop = CMD_SENSE_ID;
            break;
        // storage_write //
        case('3'):
            cmdop = CMD_STORAGE_WRITE_ID;
            break;
        // storage_read //
        case('4'):
            cmdop = CMD_STORAGE_READ_ID;
            break;
        // time_set //
        case('5'):
            cmdop = CMD_TIME_SET_ID;
            break;
        // time_get //
        case('6'):
            cmdop = CMD_TIME_GET_ID;
            break;
        // sample //
        case('7'):
            cmdop = CMD_SAMPLE_ID;
            break;
        // no_op //
        default:
            cmdop = CMD_END_ID;
            break;
    }

    return cmd_exec(cmd_def_list, cmdop, data);
}

void cmd_example() {
    enum {
        CMD_NOOP_ID = 0x00,
    };
    cmd_def_t cmd_def_list[] = {
        {.id = CMD_NOOP_ID, .handler = (cmd_handler_fn)cmd_noop},
        {.id = CMD_END_ID} // this must always be last
    };
    cmd_exec(cmd_def_list, CMD_NOOP_ID, NULL);
}