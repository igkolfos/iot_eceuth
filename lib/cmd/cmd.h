#ifndef __CMD_H__
#define __CMD_H__

//////////////////////////////////////
//         3 char  commands         //
// cmd[0]: command code             //
// cmd[1]: first parameter          //
// cmd[2]: second parameter         //
//////////////////////////////////////

#define COMMANDSIZE 3

// Max time = 23 * 3600 + 59 * 50 + 59 = 86399 sec, i.e. 5 char max
#define TIMEDATASIZE 5

extern UART_HandleTypeDef huart1;

#define TRANSMITTER_TIMEOUT 500 // ms

#define TIMEOUTBEFOREPC 2000 // 2 sec
    // use this delay in order to send all messages 
    // to PC, let it read and clear its buffers and then 
    // send the data

// MACRO to avoid compiler warnings
#define ARG_UNUSED(x) (void)(x)

#define CMD_END_ID 0xFF // the ID of the last entry in the cmd_def_t list

typedef uint8_t cmd_id_t;
typedef int (*cmd_handler_fn)(const void *data);
typedef struct {
    cmd_id_t id;
    cmd_handler_fn handler;
} cmd_def_t;

/**
 * @brief Command executor. Calls the corresponding handler based on the provided command id.
 * 
 * @param[in] list List of all commands.
 * @param[in] id The ID of the target command.
 * @param[in] data The data that will be passed to the handler.
 * @return int The result status of the operation.
 */
int cmd_exec(cmd_def_t list[], cmd_id_t id, void *data);

/**
 * @brief Example handler that does nothing.
 * 
 * @param[in] unused 
 * @return int
 */
int cmd_noop(void *unused);

/**
 * @brief Set a led on or off, based on data received from CLI
 * 
 * @param[in] data: data[0] --> led code, data[1] --> led status 
 * @return int
 */
int cmd_set_led(void *data);

/**
 * @brief Set commands and pass data to cmd_exec. Used to set up cmd commands
 */
int cmd_read_data(char *data);

void cmd_example();

#endif /* __CMD_H__ */
