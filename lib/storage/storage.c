#include <stdint.h>
#include <errno.h>
#include <string.h>
#include "main.h"
#include "storage.h"
#include "led.h"

#define ADDR_FLASH_PAGE_16    ((uint32_t)0x08010000) /* Base @ of Page 16, 4 Kbytes */

// static FLASH_EraseInitTypeDef EraseInitStruct;
// uint32_t StartPage = 0;
// uint32_t EndPage = 0;
// uint32_t Endaddress = 0;
// uint32_t Startaddress = 0;
// uint32_t PageError = 0;

/*Variable used to handle the Options Bytes*/
// static FLASH_OBProgramInitTypeDef OptionsBytesStruct, OptionsBytesStruct2;

static uint32_t GetPage(uint32_t Addr)
{
    // Taken from FLASH_WriteProtection example at STM github
    return (Addr - FLASH_BASE) / FLASH_PAGE_SIZE;
}

// int flash_64bit(uint32_t startaddress, uint64_t datatoflash, uint32_t size)
// {
//     uint32_t endaddress = startaddress + size;

//     if (size > 64)
//     {
//         LOG_ERR("Too large data to be flashed.\n");
//         return 1;
//     }

//     // LOG_INF("Program Flash @ %d = %d (# = %d)\n", startaddress, datatoflash, size);

//     while (startaddress < endaddress)
//     {
//         if (HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, startaddress, datatoflash) == HAL_OK)
//         {
//           startaddress = startaddress + 8;
//         }
//         else
//         {
//           /* Error occurred while writing data in Flash memory.
//              User can add here some code to deal with this error */
//           LOG_ERR("HAL_FLASH_Program Failed!\n");
          
//           set_led(LED_ALL, LED_ON);

//           return HAL_FLASH_GetError();
//         }

//     }
// }

void storase_erase(uint32_t Startaddress, uint32_t Endaddress)
{
    static FLASH_EraseInitTypeDef EraseInitStruct;
    uint32_t StartPage = 0;
    uint32_t EndPage = 0;
    uint32_t PageError = 0;

    // /* Unlock the Flash to enable the flash control register access *************/
    HAL_FLASH_Unlock();

    // /* Clear OPTVERR bit set on virgin samples */
    __HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_OPTVERR);

    /* Get the number of the start and end pages */
    StartPage = GetPage(Startaddress);
    EndPage   = GetPage(Endaddress);

    /* Fill EraseInit structure************************************************/
    EraseInitStruct.TypeErase   = FLASH_TYPEERASE_PAGES;
    EraseInitStruct.Page        = StartPage;
    EraseInitStruct.NbPages     = EndPage - StartPage + 1;

    // LOG_DBG("\tStartPage     = %d\n", StartPage);
    // LOG_DBG("\tEndPage       = %d\n", EndPage);
    // LOG_DBG("\tNbPages       = %d\n", EraseInitStruct.NbPages);
    LOG_DBG("\tStart Address = %d\n", Startaddress);
    LOG_DBG("\tEnd Address   = %d\n", Endaddress);

    LOG_DBG("\tErasing from Page %d to Page %d\n", StartPage, EndPage);
    if (HAL_FLASHEx_Erase(&EraseInitStruct, &PageError) != HAL_OK)
    {
        // Error occurred while page erase.
        // User can add here some code to deal with this error.
        // PageError will contain the faulty page and then to know the code error on this page,
        // user can call function 'HAL_FLASH_GetError()'

        LOG_ERR("HAL_FLASHEx_Erase Failed! PageError = %d\n", PageError);
        
        set_led(LED_ALL, LED_ON);

        /* Lock the Flash to disable the flash control register access (recommended
         to protect the FLASH memory against possible unwanted operation) *********/
        HAL_FLASH_Lock();

        return -ENOTSUP;
    }

    /* Lock the Flash to disable the flash control register access (recommended
     to protect the FLASH memory against possible unwanted operation) *********/
    HAL_FLASH_Lock();
}

// NOTE: specified idx (i.e. address) is supposed to be a small number in order
// to be transmitted easily and not let the user worry about the actual addresses;
// also in order to avoid conflicts during flash, start and end addresses are 
// always multiple of 8, i.e. 64-bit flashed each time
int storage_write(uint32_t idx, uint8_t *data, uint32_t len) {
    uint32_t StartPage = 0;
    uint32_t EndPage = 0;
    uint32_t Endaddress = 0;
    uint32_t Startaddress = 0;
    uint32_t PageError = 0;

    uint64_t datatoflash = 0; // 64-bit data to flash, filled in packages from specified data
    int datafound = 0; // flag during consistency check
    int currpackage = 0; // packages index
    int packages = 0; // number of 64-bit packages to flash
    int bytestoflash = 0; // number of bytes to copy from data to datatoflash


    // specified idx (i.e. address) is supposed to be a small number in order to be 
    // transmitted easily and not let the user worry about the actual addresses;
    // also in order to avoid conflicts during flash, start and end addresses are 
    // always multiple of 8, i.e. 64-bit flashed each time
    Startaddress = ADDR_FLASH_PAGE_16 + idx * 8;

    // Endaddress = Startaddress + len;
    Endaddress = Startaddress + len; // when data are used as integers

    /* Get the number of the start and end pages */
    StartPage = GetPage(Startaddress);
    EndPage   = GetPage(Endaddress);


    // LOG_DBG("\tStartPage     = %d\n", StartPage);
    // LOG_DBG("\tEndPage       = %d\n", EndPage);
    // LOG_DBG("\tNbPages       = %d\n", EraseInitStruct.NbPages);
    LOG_DBG("\tStart Address = %d\n", Startaddress);
    LOG_DBG("\tEnd Address   = %d\n", Endaddress);
    
    // storase_erase(Startaddress, Endaddress);

    // /* Unlock the Flash to enable the flash control register access *************/
    HAL_FLASH_Unlock();

    // /* Clear OPTVERR bit set on virgin samples */
    __HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_OPTVERR);

    // len = number of bytes to flash
    // len / 8 = number of bytes to flash
    // divide by 64 to check if they fit to datatoflash (64-bit)
    packages = (len / 8) + 1;

    /* FLASH Word program of DATA_64 at addresses defined by idx + len */
    Startaddress = ADDR_FLASH_PAGE_16 + idx * 8;

    LOG_INF("Program Flash @ %d = %s\n", Startaddress, data);    

    // setup datatoflash and flash it
    for(currpackage = 0; currpackage < packages; currpackage++)
    {
        datatoflash = 0;
        
        if (currpackage == packages - 1) // final package
        {
            bytestoflash = len % 8; // set remaining bytes
        }
        else
        {
            bytestoflash = 8; // set max, 8, bytes to datatoflash
        }

        // // fill datatoflash 64-bit variable with the appropriate data 
        memcpy(&datatoflash, data + (currpackage * 8), bytestoflash);

        // for (currentbyte = 0; currentbyte < (8 * currpackage); currentbyte++)
        // {
        //     datatoflash = datatoflash | (data[currentbyte] >> (currpackage * 8));
        // }

        LOG_DBG("\tPackage %d @ %d = %lf\n", currpackage, Startaddress, (double)datatoflash);    
        LOG_DBG("\tProgram Flash @ %d = %lf\n", Startaddress, datatoflash);

        LOG_DBG("\tProgram Flash @ %d = %d\n", Startaddress, datatoflash);

        if (HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, Startaddress, datatoflash) == HAL_OK)
        {
          Startaddress = Startaddress + 8;
        }
        else
        {
          /* Error occurred while writing data in Flash memory.
             User can add here some code to deal with this error */
          LOG_ERR("HAL_FLASH_Program Failed!\n");
          
          set_led(LED_ALL, LED_ON);

          return HAL_FLASH_GetError();
        }

        // probably unnecessary check 
        if (Startaddress >= Endaddress)
        {
            break;
        }
    }

    /* Check the correctness of written data */
    Startaddress = ADDR_FLASH_PAGE_16 + idx * 8;

    while (Startaddress < Endaddress)
    {
        if((*(__IO uint64_t*) Startaddress) == datatoflash)
        {
            LOG_DBG("\tFound @ %d data = %d!\n", Startaddress, (double)(*(__IO uint64_t*) Startaddress));
            datafound = 1;
        }

        Startaddress += 8;
    }

    if (datafound != 1)
    {
        LOG_ERR("Flash correctness failed!\n");
    }
    else
    {
        LOG_INF("ALL GOOD!\n");
    }

    /* Lock the Flash to disable the flash control register access (recommended
     to protect the FLASH memory against possible unwanted operation) *********/
    HAL_FLASH_Lock();

    return -ENOTSUP;
}

// NOTE: specified idx (i.e. address) is supposed to be a small number in order
// to be transmitted easily and not let the user worry about the actual addresses;
// also in order to avoid conflicts during flash, start and end addresses are 
// always multiple of 8, i.e. 64-bit flashed each time    
int storage_read(uint32_t idx, uint8_t *data, uint32_t len) {
    int numberofwords = 0;
    uint32_t address = 0;

    LOG_DBG("\tstorage_read() idx = %d, len = %d\n", idx, len);
    
    address = ADDR_FLASH_PAGE_16 + idx * 8;

    LOG_DBG("\tRead from address %d\n", address);
    
    numberofwords = len;
    while (1)
    {
        *data = *(__IO uint8_t *)address;
        LOG_DBG("\tdata (as c) = %c\n", *(char *) data);
        address += 1;
        data++;

        numberofwords--;
        if (numberofwords == 0)
        {
            break;
        }
    }

    *data = '\0';
    LOG_DBG("Data read = %s\n", data);

  return -ENOTSUP;
}