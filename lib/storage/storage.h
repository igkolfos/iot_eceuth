#ifndef __STORAGE_H__
#define __STORAGE_H__

#include <stdint.h>

/**
 * @brief Writes the provided data to the board's storage.
 * 
 * @param[in] idx The write index.
 * @param[in] data The data that will be written.
 * @param[in] len The length of the provided data.
 * @return int The result status of the operation. Negative means error. Non negative means
 *      the number of written bytes.
 */
int storage_write(uint32_t idx, uint8_t *data, uint32_t len);
/**
 * @brief Reads and returns data from the board's storage.
 * 
 * @param idx The read index.
 * @param data The data that were read.
 * @param len The number of bytes that the caller wants to read.
 * @return int The result status of the operation. Negative means error. Non negative means
 *      the number of read bytes.
 */
int storage_read(uint32_t idx, uint8_t *data, uint32_t len);

/**
  * @brief  Gets the page of a given address
  * @param  Addr: Address of the FLASH Memory
  * @retval The page of a given address
  */
// static uint32_t GetPage(uint32_t Addr);


// erase storage pages specified from Startaddress to Endaddress 
void storase_erase(uint32_t Startaddress, uint32_t Endaddress);

#endif /* __STORAGE_H__ */
