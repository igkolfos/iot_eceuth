#include "main.h"
#include "stm32wb55xx.h"
#include "stm32wbxx_hal_gpio.h"
#include "led.h"

// MACRO to avoid compiler warnings
#define ARG_UNUSED(x) (void)(x)

typedef struct {
	GPIO_TypeDef* port;
	uint16_t pin;
} led_entry_t;

led_entry_t led_map[LED_N] = {
	{.port = LEDR_GPIO_Port, .pin = LEDR_Pin}, // RED
	{.port = LEDG_GPIO_Port, .pin = LEDG_Pin}, // GREEN
	{.port = LEDB_GPIO_Port, .pin = LEDB_Pin}, // BLUE
};

void set_led(led_t led, led_state_t state) {
	if (led == LED_ALL) {
		set_led(LED_RED, state);
		set_led(LED_GREEN, state);
		set_led(LED_BLUE, state);
		return;
	}
	led_entry_t led_entry = led_map[led];
	// ARG_UNUSED(led_entry); // TODO: set the value of the target GPIO pin
  	
  	HAL_GPIO_WritePin(led_entry.port, led_entry.pin, state);
}