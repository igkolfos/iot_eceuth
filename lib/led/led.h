#ifndef __LED_H__
#define __LED_H__

#include "stm32wbxx_hal_gpio.h"

#define LED_N 3 // lib supports three leds (RGB)

typedef enum {
    LED_OFF = GPIO_PIN_RESET,
    LED_ON = GPIO_PIN_SET
} led_state_t;

typedef enum {
    LED_0 = 0x00,
    LED_1 = 0x01,
    LED_2 = 0x02,
    // user friendly aliases
    LED_RED = LED_0,
    LED_GREEN = LED_1,
    LED_BLUE = LED_2,
    LED_ALL = 0xFF 
} led_t;

/**
 * @brief Setter for leds.
 * 
 * @param[in] led 
 * @param[in] state
 */
void set_led(led_t led, led_state_t state);

#endif /* __LED_H__ */
